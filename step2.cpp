#include <iostream>
#include <string>

std::string name = nullptr;
int age = -1;
bool debug = false;

void parse_args(int argc, char** argv)
{
    // Parse the following arguments
    // --name {name}
    // --age {age}
    // --debug
}

int main(int argc, char** argv)
{
    parse_args(argc, argv);

    std::cout << "Result:\nName: " << name
        << "\nAge: " << age
        << "\nDebug: " << (debug?"true":"false") << std::endl;

    return 0;
}
