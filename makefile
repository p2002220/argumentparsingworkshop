step1:
	g++ -g step1.cpp -o step1

step2:
	g++ -g step2.cpp -o step2

step3:
	g++ -g step3.cpp -o step3

clean:
	rm step1 step2 step3
